import React, { Fragment, useEffect, useState } from 'react';
import { Button, Col, Container, Row, Table } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import CartDetails from '../components/CartDetails';
import { useAuthContext } from '../hooks/useAuthContext';
import { useCart } from '../hooks/useCart';
import { useCartContext } from '../hooks/useCartContext';

const Cart = () => {
  const { items, error: cartError } = useCartContext();
  const { user, token } = useAuthContext();

  const { checkout } = useCart();

  let total = 0;
  items.length > 0 && items.map((item) => (total += item.subTotal));
  return !token ? (
    <Navigate to="/" />
  ) : user && !user.isAdmin ? (
    <Container>
      <Row>
        <Col className="bg-light p-5 my-5">
          {items.length > 0 ? (
            <Fragment>
              <Table className="text-center">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Color</th>
                    <th>Size</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Sub-Total</th>
                    <th>Action</th>
                  </tr>
                </thead>
                {items.map((item) => (
                  <CartDetails key={item._id} item={item} />
                ))}
                <tfoot>
                  <tr className="h5">
                    <td colSpan={6} className="text-start ps-5">
                      Total
                    </td>
                    <td>&#8369; {total}</td>
                  </tr>
                </tfoot>
              </Table>
              <div className="d-flex justify-content-center">
                <Button as={Link} to="/" className="mx-3">
                  Shop more
                </Button>
                <Button
                  variant="success"
                  className="mx-3 border-0"
                  onClick={checkout}
                >
                  Checkout
                </Button>
              </div>
            </Fragment>
          ) : (
            <h1 className="display-3 text-center">
              No products in your cart yet!
            </h1>
          )}
        </Col>
      </Row>
    </Container>
  ) : (
    <Navigate to="/" />
  );
};

export default Cart;
