import React, { createContext, useEffect, useState } from 'react';
import { Button, Col, Form, Row, Container } from 'react-bootstrap';

import { useSignup } from '../hooks/useSignup';
import { useMultiStepForm } from '../hooks/useMultiStepForm';

import UserForm from '../components/UserForm';
import AddressForm from '../components/AddressForm';
import AccountForm from '../components/AccountForm';

const Signup = () => {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    cellNumber: '',
    gender: 'male',
    street: '',
    barangay: '',
    municipality: '0',
    province: '0',
    region: '0',
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const { signup, error, isLoading } = useSignup();

  const {
    steps,
    currentStepIndex,
    step,
    isFirstStep,
    isLastStep,
    back,
    next,
    goTo,
  } = useMultiStepForm([
    <UserForm handleChange={handleChange} formData={formData} />,
    <AddressForm handleChange={handleChange} formData={formData} />,
    <AccountForm handleChange={handleChange} formData={formData} />,
  ]);

  const [isEmpty, setIsEmpty] = useState(true);

  let index = currentStepIndex + 1;

  useEffect(() => {
    if (
      formData.barangay !== '0' &&
      formData.municipality !== '0' &&
      formData.province !== '0' &&
      formData.region !== '0' &&
      formData.street
    ) {
      setIsEmpty(false);
    } else if (
      index === 3 &&
      formData.password &&
      formData.confirmPassword &&
      formData.password === formData.confirmPassword
    ) {
      setIsEmpty(false);
    } else {
      setIsEmpty(true);
    }
  }, [formData]);

  function handleChange(e) {
    const { name, value } = e.target;

    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        [name]: value,
      };
    });
  }

  const handleSubmit = async (e) => {
    console.log(formData);
    e.preventDefault();
    next();
    if (index === 3) {
      await signup(
        formData.firstName,
        formData.lastName,
        formData.cellNumber,
        formData.gender,
        formData.street,
        formData.barangay,
        formData.municipality,
        formData.province,
        formData.region,
        formData.username,
        formData.email,
        formData.password,
        formData.confirmPassword
      );
    }
  };

  return (
    <Container>
      <Row className="justify-content-center p-5 mx-auto">
        <Col sm={12} md={8} lg={6} className="bg-light p-3 rounded">
          <Form onSubmit={handleSubmit}>
            <div>
              <div className="text-center">
                {currentStepIndex + 1} / {steps.length}
              </div>
              {step}
              <div className="text-center mt-3">
                {!isFirstStep && (
                  <Button onClick={back} className="me-2">
                    Back
                  </Button>
                )}
                {!isLastStep ? (
                  <Button
                    type="submit"
                    className="ms-2"
                    disabled={index === 2 && isEmpty}
                  >
                    Next
                  </Button>
                ) : (
                  <Button type="submit" className="ms-2" disabled={isEmpty}>
                    Finish
                  </Button>
                )}
              </div>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default Signup;
