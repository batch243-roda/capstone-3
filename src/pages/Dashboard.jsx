import React, { Fragment, useEffect, useState } from 'react';
import {
  Button,
  Col,
  Container,
  Modal,
  ModalHeader,
  Row,
  Spinner,
  Table,
} from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import DashboarProductDetails from '../components/DashboarProductDetails';
import CreateProduct from '../components/CreateProduct';
import { useAuthContext } from '../hooks/useAuthContext';
import { useProductsContext } from '../hooks/useProductsContext';

const Dashboard = () => {
  const { user, token } = useAuthContext();
  const { allProducts, dispatch: productsDispatch } = useProductsContext();
  const [show, setShow] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/products/all`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        setError(json.error);
        setIsLoading(false);
      }

      if (response.ok) {
        setIsLoading(false);
        productsDispatch({ type: 'SET_ALL_PRODUCTS', payload: json });
      }
    };

    fetchProducts();
  }, []);

  return !token ? (
    <Navigate to="/" />
  ) : user && user.isAdmin ? (
    <Container>
      <Row>
        <Col className="bg-light p-5 my-5 text-center">
          <h2 className="">Admin Dashboard</h2>
          <div>
            <Button onClick={handleShow} variant="success" className="mx-1">
              Add Product
            </Button>
            <Button variant="danger" as={Link} to="../orders" className="mx-1">
              Show Orders
            </Button>
          </div>
          {allProducts && allProducts.length > 0 ? (
            <Fragment>
              <Table
                key={allProducts._id}
                className="text-center mt-3"
                responsive
              >
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                {allProducts.map((product) => (
                  <DashboarProductDetails key={product._id} product={product} />
                ))}
              </Table>
            </Fragment>
          ) : (
            <h1 className="display-1">{error}</h1>
          )}
        </Col>
      </Row>
      <Modal show={show} onHide={handleClose} fullscreen={true}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center w-100">
            Create Product
          </Modal.Title>
        </Modal.Header>
        <CreateProduct />
      </Modal>
    </Container>
  ) : (
    <Navigate to="/" />
  );
};

export default Dashboard;
