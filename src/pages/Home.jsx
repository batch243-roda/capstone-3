import React, { useEffect, useState } from 'react';
import { useProductsContext } from '../hooks/useProductsContext';

// components
import HomeProductDetails from '../components/HomeProductDetails';
import { Button, Container, Form, Row, Spinner } from 'react-bootstrap';

const Home = () => {
  const { activeProducts, dispatch: productsDispatch } = useProductsContext();
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [query, setQuery] = useState('');

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/products?name=${query}`
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json.error);
        setError(json.error);
        setIsLoading(false);
        console.log(error);
      }

      if (response.ok) {
        productsDispatch({ type: 'SET_ACTIVE_PRODUCTS', payload: json });
        setIsLoading(false);
      }
    };

    fetchProducts();
  }, [query]);

  return (
    <Container className="my-5">
      <Form className="d-flex">
        <Form.Control
          className="me-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
          name="search"
          onChange={(e) => setQuery(e.target.value)}
          value={query}
        />
      </Form>
      <h3 className="mt-3">Recently Added</h3>
      <Row>
        {isLoading ? (
          <Spinner animation="grow" />
        ) : activeProducts && activeProducts.length > 0 ? (
          activeProducts.map((product) => (
            <HomeProductDetails key={product._id} product={product} />
          ))
        ) : (
          <h1 className="display-1 text-center mt-5">{error}</h1>
        )}
      </Row>
    </Container>
  );
};

export default Home;
