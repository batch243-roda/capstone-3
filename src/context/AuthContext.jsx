import React, { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { useRetrieveProfile } from '../hooks/useRetrieveProfile';

export const AuthContext = createContext();

export const authReducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      return { user: action.payload };
    case 'LOGOUT':
      return { user: null };
    default:
      return state;
  }
};

export const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, { user: null });
  const token = JSON.parse(localStorage.getItem('token'));

  useEffect(() => {
    const fetchUser = async () => {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/profile`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json.error);
      }

      if (response.ok) {
        dispatch({ type: 'LOGIN', payload: json });
      }
    };
    token && fetchUser();
  }, [token]);

  return (
    <AuthContext.Provider value={{ ...state, dispatch, token }}>
      {children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.object,
};
