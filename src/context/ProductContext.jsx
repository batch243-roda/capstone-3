import React, { createContext, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';

export const ProductsContext = createContext();

export const productReducer = (state, action) => {
  switch (action.type) {
    case 'SET_ACTIVE_PRODUCTS':
      return {
        activeProducts: action.payload,
      };
    case 'SET_SINGLE_PRODUCT':
      return {
        singleProduct: action.payload,
      };
    case 'SET_ALL_PRODUCTS':
      return {
        allProducts: action.payload,
      };
    case 'ARCHIVED_PRODUCT':
      const index = state.allProducts.findIndex(
        (el) => el._id === action.payload._id
      );
      state.allProducts[index] = action.payload;
      return {
        allProducts: state.allProducts,
      };
    case 'CREATE_PRODUCT':
      if (state.allProducts && state.allProducts.length > 0) {
        const prodSize = state.allProducts.length;
        for (let i = 0; i < prodSize; i++) {
          const detSize = state.allProducts[i].details.length;

          for (let j = 0; j < detSize; j++) {
            if (state.allProducts[i]._id === action.payload._id) {
              const { color, size, stocks, price } = action.payload.details[j];
              const { img_url } =
                action.payload.img_url[action.payload.img_url.length - 1];
              console.log(color, size, stocks, price, img_url);
              if (!state.allProducts[i].details[j].color === color) {
                state.allProducts[i].img_url.push(img_url);
              }
              state.allProducts[i].details.push(color, size, stocks, price);
              return state;
            }
          }
        }
      }
      return {
        allProducts: [action.payload, ...state.allProducts],
      };
    case 'UPDATE_PRODUCT':
      if (state.allProducts && state.allProducts.length > 0) {
        const prodSize = state.allProducts.length;
        for (let i = 0; i < prodSize; i++) {
          const detSize = state.allProducts[i].details.length;

          for (let j = 0; j < detSize; j++) {
            if (state.allProducts[i]._id === action.payload.productId) {
              const {
                productId,
                detailsId,
                name,
                description,
                color,
                size,
                stocks,
                price,
              } = action.payload;

              if (state.allProducts[i].details[j]._id === detailsId) {
                if (color) state.allProducts[i].details[j].color = color;
                if (size) state.allProducts[i].details[j].size = size;
                if (stocks) state.allProducts[i].details[j].stocks = stocks;
                if (price) state.allProducts[i].details[j].price = price;
                if (name) state.allProducts[i].name = name;
                if (description) state.allProducts[i].description = description;

                return state;
              }
            }
          }
        }
      }
      return {
        allProducts: [action.payload, ...state.allProducts],
      };
    case 'BUY_PRODUCT':
      for (let j = 0; j < state.singleProduct.details.length; j++) {
        if (state.singleProduct.details[j]._id === action.payload.detailsId) {
          console.log(state.singleProduct.details[j].stocks);
          state.singleProduct.details[j].stocks -= action.payload.quantity / 2;

          return state;
        }
      }
    default:
      return state;
  }
};

export const ProductContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(productReducer, {
    activeProducts: [],
    allProducts: [],
    singleProduct: null,
  });

  return (
    <ProductsContext.Provider value={{ ...state, dispatch }}>
      {children}
    </ProductsContext.Provider>
  );
};

ProductContextProvider.propTypes = {
  children: PropTypes.object,
};
