import React, { createContext, useReducer } from 'react';

export const OrderContext = createContext();

export const orderReducer = (state, action) => {
  switch (action.type) {
    case 'SET_ORDERS':
      return {
        orders: action.payload,
      };
    case 'RECEIVED_ORDERS':
      for (let i = 0; i < state.orders.length; i++) {
        if (state.orders[i]._id === action.payload) {
          state.orders[i].orderReceived = true;
          return {
            orders: state.orders,
          };
        }
      }
    case 'APPROVED_ORDERS':
      for (let i = 0; i < state.orders.length; i++) {
        if (state.orders[i]._id === action.payload) {
          state.orders[i].status = 'approved';
          return {
            orders: state.orders,
          };
        }
      }
    case 'CANCELLED_ORDERS':
      for (let i = 0; i < state.orders.length; i++) {
        if (state.orders[i]._id === action.payload) {
          state.orders[i].status = 'cancelled';
          return {
            orders: state.orders,
          };
        }
      }
    case 'APPROVED':
      return {
        orders: state.orders.filter(
          (order) => order.status === 'approved' && !order.orderReceived
        ),
      };
    case 'PENDING':
      return {
        orders: state.orders.filter((order) => order.status === 'pending'),
      };
    case 'CANCELLED':
      return {
        orders: state.orders.filter((order) => order.status === 'cancelled'),
      };
    case 'TO_RECEIVED':
      return {
        orders: state.orders.filter(
          (order) => !order.orderReceived && order.status === 'approved'
        ),
      };
    case 'RECEIVED':
      return {
        orders: state.orders.filter((order) => order.orderReceived),
      };
    default:
      return state;
  }
};

export const OrderContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(orderReducer, {
    orders: [],
  });

  return (
    <OrderContext.Provider value={{ ...state, dispatch }}>
      {children}
    </OrderContext.Provider>
  );
};
