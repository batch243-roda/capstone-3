import { useState } from 'react';
import Swal from 'sweetalert2';
import { useAuthContext } from './useAuthContext';
import { useCartContext } from './useCartContext';
import { useOrderContext } from './useOrderContext';
import { useProductsContext } from './useProductsContext';

export const useCart = () => {
  const { user, token } = useAuthContext();
  const { dispatch: cartDispatch, setError } = useCartContext();
  const { dispatch: orderDispatch } = useOrderContext();
  const { dispatch: productDispatch } = useProductsContext();

  const addCart = async (productId, quantity, color, size) => {
    const item = { productId, quantity, color, size };
    const response = await fetch(
      `${import.meta.env.VITE_LOCALHOST_API}/api/users/addToCart`,
      {
        method: 'POST',
        body: JSON.stringify(item),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      }
    );

    const json = await response.json();

    if (!response.ok) {
      setError(json.error);
      console.log(json);
      Swal.fire(`Error`, `${json.error}`, 'error');
    }

    if (response.ok) {
      setError(null);
      console.log('New item added in cart', json);
      cartDispatch({
        type: 'ADD_CART',
        payload: json.products || json[0].products,
      });
      Swal.fire(
        `Success`,
        `${
          json[0].products[0].name || json.products[0].name
        } was added successfully to your cart`,
        'success'
      );
    }
  };

  const removeCartItem = async (cartProductId) => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/removeFromCart`,
        {
          method: 'DELETE',
          body: JSON.stringify({ cartProductId }),
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const json = await response.json();

      if (!response.ok) {
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire('Deleted Successfully!', `${json}.`, 'success');
        cartDispatch({ type: 'DELETE_CART', payload: cartProductId });
      }
    }
  };

  const checkout = async () => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "Yes, i'll checkout  it!",
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/checkout`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }
      );

      const json = await response.json();
      console.log(json);
      if (!response.ok) {
        console.log('error');
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire(
          'Checkout Successfully!',
          `Thank you for Shopping 🤗.`,
          'success'
        );
        cartDispatch({ type: 'CHECKOUT', payload: json });
      }
    }
  };

  const buyNow = async (productId, color, size, quantity, price, detailsId) => {
    const total = price * quantity;
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: `Total price is ₱${total}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "Yes, i'll checkout  it!",
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/users/buyNow`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ productId, color, size, quantity }),
        }
      );

      const json = await response.json();
      if (!response.ok) {
        Swal.fire('Somethings not right!', `${json.error}`, 'error');
      }

      if (response.ok) {
        Swal.fire(
          'Bought Successfully!',
          `Thank you for Shopping 🤗.`,
          'success'
        );
        console.log('Item bought:', json);
        cartDispatch({ type: 'CHECKOUT', payload: json });
        productDispatch({
          type: 'BUY_PRODUCT',
          payload: { productId, quantity, detailsId },
        });
      }
    }
  };

  return { user, addCart, removeCartItem, checkout, buyNow };
};
