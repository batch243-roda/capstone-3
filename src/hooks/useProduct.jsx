import React, { useState } from 'react';
import Swal from 'sweetalert2';
import { useAuthContext } from './useAuthContext';
import { useProductsContext } from './useProductsContext';

export const useProduct = () => {
  const { dispatch: productsDispatch } = useProductsContext();
  const { token } = useAuthContext();

  const createProduct = async (
    name,
    description,
    color,
    size,
    stocks,
    price,
    img_url,
    main,
    sub
  ) => {
    const response = await fetch(
      `${import.meta.env.VITE_LOCALHOST_API}/api/products/create`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          name,
          description,
          color,
          size,
          stocks,
          price,
          img_url,
          main,
          sub,
        }),
      }
    );

    const json = await response.json();

    if (!response.ok) {
      Swal.fire(`Sorry!`, `${json.error}`, 'error');
    }

    if (response.ok) {
      console.log('New item added in product', json[0] || json);
      productsDispatch({ type: 'CREATE_PRODUCT', payload: json[0] || json });
      Swal.fire(
        `Congrats!`,
        `Product ${name} created successfully!`,
        'success'
      );
    }
  };

  const updateProduct = async (
    productId,
    detailsId,
    name,
    description,
    color,
    size,
    stocks,
    price
  ) => {
    const response = await fetch(
      `${import.meta.env.VITE_LOCALHOST_API}/api/products/update`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          productId,
          detailsId,
          name,
          description,
          color,
          size,
          stocks,
          price,
        }),
      }
    );

    const json = await response.json();

    if (!response.ok) {
      Swal.fire(`Sorry!`, `${json.error}`, 'error');
    }

    if (response.ok) {
      console.log('New item added in product', json[0] || json);
      productsDispatch({
        type: 'UPDATE_PRODUCT',
        payload: {
          productId,
          detailsId,
          name,
          description,
          color,
          size,
          stocks,
          price,
        },
      });
      Swal.fire(
        `Congrats!`,
        `Product ${name} updated successfully!`,
        'success'
      );
    }
  };

  const archivedUnarchived = async (id) => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, do it!',
    });

    if (result.isConfirmed) {
      const response = await fetch(
        `${import.meta.env.VITE_LOCALHOST_API}/api/products/archived`,
        {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ productId: id }),
        }
      );

      const json = await response.json();

      if (!response.ok) {
        console.log(json.error);
      }

      if (response.ok) {
        productsDispatch({ type: 'ARCHIVED_PRODUCT', payload: json });
      }
    }
  };

  return { archivedUnarchived, createProduct, updateProduct };
};
