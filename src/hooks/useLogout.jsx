import Swal from 'sweetalert2';
import { useAuthContext } from './useAuthContext';
import { useCartContext } from './useCartContext';

export const useLogout = () => {
  const { dispatch: authDispatch } = useAuthContext();
  const { dispatch: cartDispatch } = useCartContext();

  const logout = () => {
    // remove user from local storage
    localStorage.removeItem('token');

    authDispatch({ type: 'LOGOUT' });
    cartDispatch({ type: 'CHECKOUT' });

    Swal.fire({
      title: 'Thank you for shopping',
      icon: 'info',
      text: 'Comeback again 😇',
    });
  };

  return { logout };
};
