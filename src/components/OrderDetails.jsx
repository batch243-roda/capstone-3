import React, { Fragment } from 'react';

const OrderDetails = ({ product }) => {
  return (
    <tr>
      <td>
        <img src={product.img_url} />
      </td>
      <td>{product.name}</td>
      <td>{product.color}</td>
      <td>{product.size}</td>
      <td>{product.quantity}</td>
      <td>&#8369; {product.price}</td>
      <td>&#8369; {product.subTotal}</td>
    </tr>
  );
};

export default OrderDetails;
