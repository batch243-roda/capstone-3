import React, { useEffect, useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useProduct } from '../hooks/useProduct';

const CreateProduct = () => {
  const [images, setImages] = useState([]);
  const [hasUploaded, setHasUploaded] = useState(false);
  const [subCat, setSubCat] = useState('');

  const { createProduct } = useProduct();

  const [formData, setFormData] = useState({
    name: '',
    description: '',
    color: '',
    size: '',
    stocks: 1,
    price: 0,
    img_url: images.img_url,
    main: "Men's Headwear",
    sub: [],
  });

  const handleRemoveImg = async (imgObj) => {
    const response = await fetch(
      `${import.meta.env.VITE_LOCALHOST_API}/api/products/removeImage`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ public_id: imgObj.public_id }),
      }
    );

    const json = await response.json();

    if (!response.ok) {
      console.log(json.error);
    }

    if (response.ok) {
      console.log(json);
      setHasUploaded(false);
      setImages((prev) =>
        prev.filter((img) => img.public_id !== imgObj.public_id)
      );
    }
  };

  const handleOpenWidget = () => {
    const myWidget = window.cloudinary.createUploadWidget(
      {
        cloudName: 'dffgxwgwn',
        uploadPreset: 'uqqxqon7',
        sources: ['local', 'unsplash', 'url', 'dropbox'],
        googleApiKey: '<image_search_google_api_key>',
        showAdvancedOptions: false,
        cropping: false,
        multiple: false,
        defaultSource: 'local',
        styles: {
          palette: {
            window: '#10173a',
            sourceBg: '#20304b',
            windowBorder: '#7171D0',
            tabIcon: '#79F7FF',
            inactiveTabIcon: '#8E9FBF',
            menuIcons: '#CCE8FF',
            link: '#72F1FF',
            action: '#5333FF',
            inProgress: '#00ffcc',
            complete: '#33ff00',
            error: '#cc3333',
            textDark: '#000000',
            textLight: '#ffffff',
          },
          fonts: {
            default: null,
            "'Merriweather', serif": {
              url: 'https://fonts.googleapis.com/css?family=Merriweather',
              active: true,
            },
          },
        },
      },
      (error, result) => {
        if (!error && result && result.event === 'success') {
          console.log('Done! Here is the image info: ', result.info);
          setHasUploaded(true);
          setFormData((prevForm) => {
            return {
              ...prevForm,
              img_url: result.info.url,
            };
          });
          setImages((prev) => [
            ...prev,
            { img_url: result.info.url, public_id: result.info.public_id },
          ]);
        }
      }
    );
    myWidget.open();
  };

  useEffect(() => {
    setFormData((prevFormData) => {
      return { ...prevFormData, sub: subCat.split(',') };
    });
  }, [subCat]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    setFormData((prevFormData) => {
      return {
        ...prevFormData,
        [name]: value,
      };
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
    createProduct(
      formData.name,
      formData.description,
      formData.color,
      formData.size,
      formData.stocks,
      formData.price,
      formData.img_url,
      formData.main,
      formData.sub
    );
  };

  return (
    <Container className="p-3">
      <Row className="mt-3">
        <Col
          md={6}
          className="d-flex flex-column justify-content-center align-items-center border rounded py-3"
        >
          {!hasUploaded && <Button onClick={handleOpenWidget}>Upload</Button>}

          {hasUploaded && (
            <div>
              <img
                src={images[0].img_url}
                width="350px"
                height="350px"
                className="rounded"
              />
            </div>
          )}
          {hasUploaded && (
            <Button
              className="bg-danger d-block border-danger mt-3"
              onClick={() => handleRemoveImg(images[0])}
            >
              Remove Image
            </Button>
          )}
        </Col>

        <Col md={6} className="px-5">
          <Form onSubmit={handleSubmit}>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              placeholder="Enter name"
              onChange={handleChange}
              value={formData.name}
              required
            />

            <Form.Label className="mt-2">Description</Form.Label>
            <Form.Control
              type="text"
              name="description"
              placeholder="Enter description"
              onChange={handleChange}
              value={formData.description}
              required
            />

            <Form.Group className="d-flex justify-content-between mt-2">
              <Col md={5}>
                <Form.Label>Color</Form.Label>
                <Form.Control
                  type="text"
                  name="color"
                  placeholder="Enter color"
                  onChange={handleChange}
                  value={formData.color}
                  required
                />
              </Col>

              <Col md={5}>
                <Form.Label>Size</Form.Label>
                <Form.Control
                  type="text"
                  name="size"
                  placeholder="Enter size"
                  onChange={handleChange}
                  value={formData.size}
                  required
                />
              </Col>
            </Form.Group>

            <Form.Group className="d-flex justify-content-between mt-2">
              <Col md={5}>
                <Form.Label>Stocks</Form.Label>
                <Form.Control
                  type="number"
                  name="stocks"
                  placeholder="Enter stocks"
                  onChange={handleChange}
                  value={formData.stocks}
                  required
                />
                <Form.Text className="text-muted d-block">
                  Stocks should not be less than 1
                </Form.Text>
              </Col>
              <Col md={5}>
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  placeholder="Enter price"
                  onChange={handleChange}
                  value={formData.price}
                  required
                />
              </Col>
            </Form.Group>

            <Form.Label>Main Category</Form.Label>
            <Form.Select
              name="main"
              onChange={handleChange}
              value={formData.main}
            >
              <option>Men's Headwear</option>
              <option>Women's Headwear</option>
              <option>Unisex Headwear</option>
              <option>Men's Apparel</option>
              <option>Women's Apparel</option>
              <option>Unisex's Apparel</option>
              <option>Men's Footwear</option>
              <option>Women's Footwear</option>
              <option>Unisex Footwear</option>
            </Form.Select>

            <Form.Label className="mt-2">Sub Category</Form.Label>
            <Form.Control
              type="text"
              name="sub"
              placeholder="Ex: Short, Slipper, Sandals"
              onChange={(e) => setSubCat(e.target.value)}
              value={subCat}
              required
            />
            <Button
              type="submit"
              className="mt-2 col-12 mx-auto bg-success border-success"
            >
              Create
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default CreateProduct;
