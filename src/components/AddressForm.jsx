import React, { Fragment } from 'react';
import { Form, Stack } from 'react-bootstrap';
import { provinces } from '../assets/address_data/provinces';
import { regions } from '../assets/address_data/regions';
import { municipalities } from '../assets/address_data/municipalities';
import { barangays } from '../assets/address_data/barangays';

function AddressForm({ handleChange, formData }) {
  return (
    <Fragment>
      <h3 className="text-center">Address</h3>
      <Form.Label>Region</Form.Label>
      <Form.Select
        name="region"
        onChange={handleChange}
        value={formData.region}
        required
      >
        <option value="0">--SELECT REGION--</option>
        {regions.map((region) => (
          <option key={region.code} value={region.code}>
            {region.name}
          </option>
        ))}
      </Form.Select>

      <Form.Label className="mt-3">Province</Form.Label>
      <Form.Select
        name="province"
        onChange={handleChange}
        value={formData.province}
        disabled={formData.region === '0'}
        required
      >
        <option value="0">--SELECT PROVINCE--</option>
        {provinces.map(
          (province) =>
            province.regionCode === formData.region && (
              <option key={province.code} value={province.code}>
                {province.name}
              </option>
            )
        )}
      </Form.Select>

      <Form.Label className="mt-3">Cities/Municipalities</Form.Label>
      <Form.Select
        name="municipality"
        onChange={handleChange}
        value={formData.municipality}
        disabled={formData.province === '0' || formData.region === '0'}
        required
      >
        <option value="0">--SELECT CITIES--</option>
        {municipalities.map(
          (municipality) =>
            municipality.provinceCode === formData.province && (
              <option key={municipality.code} value={municipality.code}>
                {municipality.name}
              </option>
            )
        )}
      </Form.Select>

      <Form.Label className="mt-3">Barangay</Form.Label>
      <Form.Select
        name="barangay"
        onChange={handleChange}
        value={formData.barangay}
        disabled={
          formData.municipality === '0' ||
          formData.province === '0' ||
          formData.region === '0'
        }
        required
      >
        <option value="0">--SELECT BARANGAY--</option>
        {barangays.map((barangay) => {
          return barangay.cityCode === false
            ? barangay.municipalityCode === formData.municipality && (
                <option key={barangay.code} value={barangay.code}>
                  {barangay.name}
                </option>
              )
            : barangay.cityCode === formData.municipality && (
                <option key={barangay.code} value={barangay.code}>
                  {barangay.name}
                </option>
              );
        })}
      </Form.Select>

      <Form.Label className="mt-3">House# / Street</Form.Label>
      <Form.Control
        type="text"
        name="street"
        placeholder="B14, A. Ph Street"
        onChange={handleChange}
        value={formData.street}
        required
      />
    </Fragment>
  );
}

export default AddressForm;
