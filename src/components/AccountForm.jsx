import React, { Fragment } from 'react';
import { Form } from 'react-bootstrap';

function AccountForm({ handleChange, formData }) {
  return (
    <Fragment>
      <Form.Label>Username</Form.Label>
      <Form.Control
        type="text"
        name="username"
        placeholder="Enter username"
        onChange={handleChange}
        value={formData.username}
        required
      />

      <Form.Label className="mt-3">Email address</Form.Label>
      <Form.Control
        type="email"
        name="email"
        placeholder="Enter email"
        onChange={handleChange}
        value={formData.email}
        required
      />
      <Form.Text className="text-muted d-block">
        We'll never share your email with anyone else.
      </Form.Text>

      <Form.Label className="mt-3">Password</Form.Label>
      <Form.Control
        type="password"
        name="password"
        placeholder="Sample@1"
        onChange={handleChange}
        value={formData.password}
        required
      />
      <Form.Text className="text-muted d-block">
        Password must be eight-character that contains at least one uppercase,
        lowercase, number, special character
      </Form.Text>

      <Form.Label className="mt-3">Confirm Password</Form.Label>
      <Form.Control
        type="password"
        name="confirmPassword"
        placeholder="Confirm Password"
        onChange={handleChange}
        value={formData.confirmPassword}
        required
      />
    </Fragment>
  );
}

export default AccountForm;
