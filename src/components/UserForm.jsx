import React, { Fragment } from 'react';
import { Form, Stack } from 'react-bootstrap';

function UserForm({ handleChange, formData }) {
  return (
    <Fragment>
      <h3 className="text-center">User Details</h3>
      <Form.Group className="">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          name="firstName"
          placeholder="Enter first name"
          onChange={handleChange}
          value={formData.firstName}
          autoFocus
          required
        />

        <Form.Label className="mt-3">Last Name</Form.Label>
        <Form.Control
          type="text"
          name="lastName"
          placeholder="Enter last name"
          onChange={handleChange}
          value={formData.lastName}
          required
        />
      </Form.Group>

      <Form.Group className="mt-3 d-flex">
        <Stack>
          <Form.Label>Cellphone Number</Form.Label>
          <Form.Control
            type="number"
            name="cellNumber"
            placeholder="+639123456789"
            className="w-75"
            onChange={handleChange}
            value={formData.cellNumber}
            required
          />
        </Stack>

        <Stack>
          <Form.Label>Sex</Form.Label>
          <Form.Select
            name="gender"
            onChange={handleChange}
            value={formData.gender}
          >
            <option value="male">Male</option>
            <option value="female">Female</option>
          </Form.Select>
        </Stack>
      </Form.Group>
    </Fragment>
  );
}

export default UserForm;
