import React from 'react';
import {
  Badge,
  Button,
  Container,
  Dropdown,
  Nav,
  Navbar,
} from 'react-bootstrap';
import { useAuthContext } from '../hooks/useAuthContext';
import {
  faCartShopping,
  faDoorOpen,
  faShoppingBag,
  faTable,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useCartContext } from '../hooks/useCartContext';
import { Link } from 'react-router-dom';
import { useLogout } from '../hooks/useLogout';

const AppNavbar = () => {
  const { user } = useAuthContext();
  const { items } = useCartContext();
  const { logout } = useLogout();

  return (
    <Navbar bg="light">
      <Container>
        <Navbar.Brand as={Link} to="/" className="fw-bolder">
          ESON&apos;s RTW
        </Navbar.Brand>

        {!user && (
          <Nav className="ms-auto">
            <Nav.Link as={Link} to="/login">
              Login
            </Nav.Link>

            <Nav.Link as={Link} to="/signup">
              Signup
            </Nav.Link>
          </Nav>
        )}

        {user && !user.isAdmin && (
          <Nav className="ms-auto">
            <Nav.Link as={Link} to="/cart">
              <FontAwesomeIcon icon={faCartShopping} className="cart-icon" />
              <Badge bg="danger" className="float-end text-light p-1">
                {items.length}
              </Badge>
            </Nav.Link>

            <Nav.Item>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  {user.username}
                </Dropdown.Toggle>

                <Dropdown.Menu className="dropdown-menu-end mt-2">
                  <Dropdown.Item as={Link} to="/profile">
                    {user.gender === 'male' ? (
                      <img
                        src="/images/users-icon/male.svg"
                        className="drop-down-icon me-2"
                        alt="male-profile"
                      />
                    ) : (
                      <img
                        src="/images/users-icon/female.svg"
                        alt="female-profile"
                      />
                    )}
                    Profile
                  </Dropdown.Item>

                  <Dropdown.Divider />

                  <Dropdown.Item as={Link} to="/orders">
                    <FontAwesomeIcon
                      icon={faShoppingBag}
                      className="drop-down-icon me-2"
                    />
                    Order
                  </Dropdown.Item>

                  <Dropdown.Divider />

                  <Dropdown.Item as={Button} onClick={logout}>
                    <FontAwesomeIcon
                      icon={faDoorOpen}
                      className="drop-down-icon me-2"
                    />
                    Logout
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Nav.Item>
          </Nav>
        )}

        {user && user.isAdmin && (
          <Nav className="ms-auto">
            <Nav.Item>
              <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  {user.username}
                </Dropdown.Toggle>

                <Dropdown.Menu className="dropdown-menu-end">
                  <Dropdown.Item as={Link} to="/profile">
                    {user.gender === 'male' ? (
                      <img
                        src="/images/users-icon/male.svg"
                        className="drop-down-icon me-2"
                        alt="male-profile"
                      />
                    ) : (
                      <img
                        src="/images/users-icon/female.svg"
                        alt="female-profile"
                      />
                    )}
                    Profile
                  </Dropdown.Item>

                  <Dropdown.Divider />

                  <Dropdown.Item as={Link} to="/dashboard">
                    <FontAwesomeIcon
                      icon={faTable}
                      className="drop-down-icon me-2"
                    />
                    Dashboard
                  </Dropdown.Item>

                  <Dropdown.Divider />

                  <Dropdown.Item as={Link} to="/orders">
                    <FontAwesomeIcon
                      icon={faShoppingBag}
                      className="drop-down-icon me-2"
                    />
                    Order
                  </Dropdown.Item>

                  <Dropdown.Divider />

                  <Dropdown.Item as={Link} on="/" onClick={logout}>
                    <FontAwesomeIcon
                      icon={faDoorOpen}
                      className="drop-down-icon me-2"
                    />
                    Logout
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Nav.Item>
          </Nav>
        )}
      </Container>
    </Navbar>
  );
};

export default AppNavbar;
