import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useAuthContext } from '../hooks/useAuthContext';
import { Button, Card, Col } from 'react-bootstrap';

function HomeProductDetails({ product }) {
  const { user } = useAuthContext();
  const priceSet = new Set();
  let priceRange = [];

  for (let i = 0; i < product.details.length; i++) {
    priceSet.add(product.details[i].price);
  }

  priceRange = Array.from(priceSet);

  return (
    <Col lg={4} md={6} sm={12} className="h-100 mt-3">
      <Card style={{ width: '18rem' }} className="mx-auto">
        <Link to={`product/${product._id}`}>
          <Card.Img variant="top" src={product.img_url[0] || product.img_url} />
        </Link>
        <Card.Body>
          <Card.Title>{product.name}</Card.Title>
          <Card.Text>{product.description.slice(0, 59)}...</Card.Text>
          <Card.Text>
            <img
              src="images/ratings/empty-star.png"
              alt="empty-star"
              className="me-1"
            />
            <img
              src="images/ratings/empty-star.png"
              alt="empty-star"
              className="me-1"
            />
            <img
              src="images/ratings/empty-star.png"
              alt="empty-star"
              className="me-1"
            />
            <img
              src="images/ratings/empty-star.png"
              alt="empty-star"
              className="me-1"
            />
            <img src="images/ratings/empty-star.png" alt="empty-star" />
          </Card.Text>
          {priceRange.length < 2 ? (
            <Card.Text>&#8369;{priceRange[0]}</Card.Text>
          ) : (
            <Card.Text>
              &#8369;{priceRange[0]} - &#8369;
              {priceRange[priceRange.length - 1]}
            </Card.Text>
          )}

          {user && (
            <Button as={Link} to={`product/${product._id}`} className="col-12">
              See More Details
            </Button>
          )}
        </Card.Body>
      </Card>
    </Col>
  );
}

export default HomeProductDetails;
